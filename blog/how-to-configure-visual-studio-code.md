---
title: How to configure Visual Studio Code right the first time
description: VS Code is a powerful tool but "batteries included" is not the expression this IDE is known for. Here's how to proceed to have the best developer experience.
date: 2020-08-20T19:24:03.035Z
---
When it comes to choosing an IDE choose wisely because a certain career-defining moment is secretly unveiling upon you. Should you go with Sublime, Webstorm, Atom, or VS Code? Obviously, there's no sorting hat to decide for you and you can change an IDE as many times as you want but have in mind that only one of them will be your favorite. I hope you choose VS Code because otherwise reading the rest of this post is pointless.
## Visual Studio Code - Insiders version
There's actually an option or 'flavor' when choosing a VS Code as an editor. you can download the stable release or you can opt to use the latest build released for the earliest of adopters - the Insiders version. The choice is up to you but if you're reading this post then it's rather obvious you should go with the regular.
## Extensions
Ok, let's get down to business. First up - Extensions! I categorized them into groups. *Essentials*, in my opinion, are required to conduct any kind of coding and stay sane. *Strongly recommended* extensions are welcome, but some of you might find them unnecessary or annoying. You can easily live without *Recommended* - they add value to your workflow but it's a very specific kind of workflow. Who knows? Maybe they work only for me?

### Essential
- Auto Rename Tag - It actually cuts in half the time required to edit two HTML tags!
- Bracket Pair Colorizer - Enables color-coding of your brackets which helps to distinguish one scope from another.
- Color Highlight - Gives you a hint of what color you've just typed in.
- DotENV - Enables syntax highlight of your secrets in the .env files.
- ESLint - This thing (if well configured) will save your butt. Hands down the biggest time-saver on this list. It finds problems in your code and it fixes them most of the time. Unless you're coding in TypeScript. Then it just sits there and does nothing.
- JavaScript (ES6) code snippets - Less typing, more time to debug.
- Prettier - Not as useful as ESLint but if configured together they actually make your codebase consistent and... Pretty.
### Strongly recommended
- Better Comments - If you comment on your code then this extension will make your comments shine! Really, TODOs, JSDocs, etc. They will look amazing.
- Error Lens - When you eventually screw something up your IDE will not only add a squiggly red underline to your code but also provide the error definition *inline*.
- GitLens - Similar thing as above only this time it's the details about the given line git history. Also provides an additional menu in your activity bar.
- Live Server - Launches a development server and opens up a port to check your work in the browser. Live-reload included!
- npm Intellisense - Helps out typing in those npm packages.
- Path Intellisense - Similarly to the above but for files.
- Settings Sync - Do you use VS Code on more than one machine and have trouble syncing your IDE between them? This one is for you. It stores your configs, extensions, etc. in a gist and enables you to download them to any machine you're currently using.
- Turbo Console Log - No more `cosnole.log' typos! With this extension, your logs will be inserted instantly, flawlessly, and elaborately.
### Recommended
- Code Spell Checker - Helps with that pesky typos. Of course, a liner will make sure your code works, but this one just takes care of the orthography exclusively. You'll need to add some exotic words to your dictionary though.
- Night Owl / Cobalt2 Theme Official - VS Code basic color theme is okay I guess. But if you want to spice things up I recommend one of those.
- Image preview - Gives you a nice inline thumbnail of and image the path to which you've just typed in.
- Kite Autocomplete for Python and JavaScript - This one's big. It requires additional stuff to be installed on your machine and used mAChiNe LeARninG to predict what you're about to type in. In the end, it saves keystrokes.
- Material Icon Theme - I don't know why but I just like to see nice colorful icons in that project tree.
- Polacode - makes a professional screenshot of your code. Looks great on twitter.
- Sort lines - It sorts your lines. Ascending, descending, case sensitive you name it
## Bonus round - tool-specific extensions
Here I gathered extensions which definitely aid only my workflow due to being framework or tool-specific.
- GraphQL for VSCode -  Syntax highlighting and linting of your GraphQL queries.
- IntelliJ IDEA Keybindings - That's a thing people will make fun of me for. I used Webstorm exclusively at work and I have grown very fond of its keymap.
- Tailwind CSS Intellisense - Recently I fell in love with TailwindCSS and this helps with typing in those utils
- Vetur - This is for Vue.js and its ecosystem
- Vue VS Code Snippets - Never too much of those snippets!

## Conclusions

There aren't any. Just pick what you like from the list above and keep on coding!
